﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Plugins.Advanced.UI
{
    [Flags]
    public enum FeatureInsertPosition
    {
        None = 0,
        Default = 1,
        Top = 2,
        Bottom = 3,
    }
}
