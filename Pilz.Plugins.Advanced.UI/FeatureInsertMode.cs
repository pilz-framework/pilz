﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Plugins.Advanced.UI
{
    [Flags]
    public enum FeatureInsertMode
    {
        /// <summary>
        /// Features will be inserted at the end of the collection.
        /// </summary>
        Default = 0,
        /// <summary>
        /// Features will be inserted at the end of the collection.
        /// This is the default behavior and equals <see cref="Default"/>. Will only be used if not set <see cref="UseCustomDefault"/>.
        /// </summary>
        DefaultEnd = Default,
        /// <summary>
        /// Features will be inserted at the start of the collection.
        /// Will only be used if not set <see cref="UseCustomDefault"/>.
        /// </summary>
        DefaultStart = 1,
        /// <summary>
        /// Features with prioritization <see cref="FeaturePrioritization.High"/> will be inserted at the top (or left).
        /// </summary>
        InsertTop = 1 << 2,
        /// <summary>
        /// Features with prioritization <see cref="FeaturePrioritization.Low"/> will be inserted at the bottom (or right).
        /// </summary>
        InsertBottom = 1 << 3,
        /// <summary>
        /// Features with prioritization other then <see cref="FeaturePrioritization.Default"/> will be inserted at the top or bottom.
        /// </summary>
        InsertTopAndBottom = InsertTop | InsertBottom,
    }
}
