﻿Imports System.Drawing.Drawing2D

Public Class DefaultLineCapProps
    Inherits LineCapProps

    Public Property LineCap As LineCap = LineCap.Flat
    Public Property CustomLineCap As CustomLineCap = Nothing

    Friend Overrides Function Configure() As LineCapConfigurationArgs
        Return New LineCapConfigurationArgs(LineCap, CustomLineCap)
    End Function

End Class
