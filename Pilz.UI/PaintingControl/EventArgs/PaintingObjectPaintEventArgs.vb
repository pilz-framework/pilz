﻿Imports System.Drawing
Imports System.IO
Imports System.Windows.Forms
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq

Public Class PaintingObjectPaintEventArgs
    Inherits EventArgs

    ''' <summary>
    ''' The Painting Object to draw.
    ''' </summary>
    ''' <returns></returns>
    Public ReadOnly Property PaintingObject As PaintingObject

    ''' <summary>
    ''' The current offset of the page on the screen.
    ''' </summary>
    ''' <returns></returns>
    Public ReadOnly Property Offset As PointF

    ''' <summary>
    ''' The Grpahics from the parent PaintingControl.
    ''' </summary>
    ''' <returns></returns>
    Public ReadOnly Property Graphics As Graphics

    ''' <summary>
    ''' The position of the PaintingObject on Screen.
    ''' </summary>
    ''' <returns></returns>
    Public ReadOnly Property Location As PointF
        Get
            Return New PointF(X, Y)
        End Get
    End Property

    ''' <summary>
    ''' The X position of the PaintingObject on Screen.
    ''' </summary>
    ''' <returns></returns>
    Public ReadOnly Property X As Single
        Get
            Return PaintingObject.X - Offset.X
        End Get
    End Property

    ''' <summary>
    ''' The Y position of the PaintingObject on Screen.
    ''' </summary>
    ''' <returns></returns>
    Public ReadOnly Property Y As Single
        Get
            Return PaintingObject.Y - Offset.Y
        End Get
    End Property

    ''' <summary>
    ''' The rectangle of the PaintingObject on Screen.
    ''' </summary>
    ''' <returns></returns>
    Public ReadOnly Property Rectangle As RectangleF
        Get
            Return New RectangleF(X, Y, PaintingObject.Width, PaintingObject.Height)
        End Get
    End Property

    Friend Sub New(obj As PaintingObject, g As Graphics)
        Me.New(obj, g, obj.Parent.Offset)
    End Sub

    Friend Sub New(obj As PaintingObject, g As Graphics, offset As PointF)
        PaintingObject = obj
        Me.Offset = offset
        Graphics = g
    End Sub

End Class
