﻿Imports System.Drawing
Imports System.Drawing.Drawing2D

Public Class ArrowLineCapProps
    Inherits LineCapProps

    Public Property Size As New Size(10, 10)
    Public Property IsFilles As Boolean = True

    Friend Overrides Function Configure() As LineCapConfigurationArgs
        Dim cap As New AdjustableArrowCap(Size.Width, Size.Height, IsFilles)
        Return New LineCapConfigurationArgs(cap)
    End Function

End Class
