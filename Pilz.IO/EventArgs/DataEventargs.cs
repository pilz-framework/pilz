﻿using System;

namespace Pilz.IO
{
    public class DataEventArgs : EventArgs
    {
        public readonly byte[] Data;

        public DataEventArgs(byte[] bytes) : base()
        {
            Data = bytes;
        }
    }
}