﻿using System.Drawing;
using System.Reflection;
using Telerik.WinControls;
using Telerik.WinControls.Elements;
using Telerik.WinControls.UI;

namespace Pilz.Plugins.Advanced.UI.Telerik
{
    public static class Extensions
    {
        public static Icon? ToIcon(this Image image)
        {
            if (image is Bitmap bitmap)
                return Icon.FromHandle(bitmap.GetHicon());
            return null;
        }

        public static RadMenuItem GetAsItem(this PluginModuleBase module)
        {
            return GetAsItem(module, true);
        }

        public static RadMenuItem GetAsItem(this PluginModuleBase module, bool addDefaultHandler)
        {
            return GetAsItem(module, addDefaultHandler ? RadMenuItem_RMMethod_Click : null);
        }

        public static RadMenuItem GetAsItem(this PluginFunction function)
        {
            return GetAsItem(function, true);
        }

        public static RadMenuItem GetAsItem(this PluginFunction function, bool addDefaultHandler)
        {
            return GetAsItem(function, addDefaultHandler ? RadMenuItem_RMFunction_Click : null);
        }

        public static RadMenuItem GetAsItem(this PluginFeature module, EventHandler? clickHandler)
        {
            var item = new RadMenuItem
            {
                Text = module.Name,
                Image = module.Icon as Image,
                SvgImage = module.Icon as RadSvgImage,
                Tag = module,
                Visibility = module.Enabled ? ElementVisibility.Visible : ElementVisibility.Collapsed
            };

            if (clickHandler is not null)
                item.Click += clickHandler;

            return item;
        }

        /// <summary>
        /// Inserts all items to an item collection.
        /// </summary>
        /// <param name="features"></param>
        /// <param name="itemsCollection">Examples:<br/>
        /// - <see cref="RadMenuItem.Items"/><br/>
        /// - <see cref="RadSplitButtonElement.Items"/><br/>
        /// - <see cref="RadDropDownButtonElement.Items"/><br/>
        /// - <see cref="RadContextMenu.Items"/><br/>
        /// - <see cref="RadRibbonBarGroup.Items"/><br/></param>
        /// <param name="addDefaultHandler">Will add a default click handler that executes the feature.<br/>
        /// You usually don't set customClickHandler if you set this parameter to <see cref="true"/>.</param>
        /// <param name="customClickHandler">Adds a custom click handler. If addDefaultHandler is true, it will only work on <see cref="PluginFeature"/>s.<br/>
        /// You usually don't set addDefaultHandler to true if you set this parameter to something not null.</param>
        public static void InsertItemsTo(this IEnumerable<PluginFeature> features, RadItemOwnerCollection itemsCollection, bool addDefaultHandler = false, EventHandler? customClickHandler = null, FeatureInsertMode insertMode = FeatureInsertMode.Default, int? customDefault = null, int? customTop = null, int? customBottom = null, FeatureInsertPosition insertSplitter = FeatureInsertPosition.None)
        {
            var insertDefault = customDefault ?? (insertMode.HasFlag(FeatureInsertMode.DefaultStart) ? 0 : itemsCollection.Count);
            var insertTop = customTop ?? (insertMode.HasFlag(FeatureInsertMode.InsertTop) || insertMode.HasFlag(FeatureInsertMode.DefaultStart) ? 0 : insertDefault);
            var insertBottom = customBottom ?? (insertMode.HasFlag(FeatureInsertMode.InsertBottom) || insertMode.HasFlag(FeatureInsertMode.DefaultEnd) ? itemsCollection.Count : insertDefault);

            foreach (var feature in features)
            {
                RadMenuItem item;

                if (feature is PluginFunction function)
                    item = function.GetAsItem(addDefaultHandler);
                else if (feature is PluginModuleBase module)
                    item = module.GetAsItem(addDefaultHandler);
                else
                    item = feature.GetAsItem(null);

                if (!addDefaultHandler && customClickHandler != null)
                    item.Click += customClickHandler;

                switch (feature.Prioritization)
                {
                    case FeaturePrioritization.High:
                        if (insertDefault >= insertTop) insertDefault++;
                        if (insertBottom >= insertTop) insertBottom++;
                        // ...
                        itemsCollection.Insert(insertTop++, item);
                        break;
                    case FeaturePrioritization.Default:
                        if (insertBottom >= insertDefault) insertBottom++;
                        if (insertTop >= insertDefault) insertTop++;
                        // ...
                        itemsCollection.Insert(insertDefault++, item);
                        break;
                    case FeaturePrioritization.Low:
                        if (insertTop >= insertBottom) insertTop++;
                        if (insertDefault >= insertBottom) insertDefault++;
                        // ...
                        itemsCollection.Insert(insertBottom++, item);
                        break;
                }
            }
        }

        private static void RadMenuItem_RMMethod_Click(object? sender, EventArgs e)
        {
            if (sender is RadMenuItem item && item.Tag is PluginModule function)
                function.ShowUI();
        }

        private static void RadMenuItem_RMFunction_Click(object? sender, EventArgs e)
        {
            if (sender is RadMenuItem item && item.Tag is PluginFunction function)
                function.Execute();
        }
    }
}