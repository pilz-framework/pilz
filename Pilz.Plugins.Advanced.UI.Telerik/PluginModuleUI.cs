﻿using Pilz.UI.Telerik.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pilz.Plugins.Advanced.UI.Telerik
{
    public class PluginModuleUI : FlyoutBase, ILoadContent
    {
        public PluginModuleUI()
        {
            ActionPanelVisible = false;
        }

        public virtual void LoadContent()
        {
        }
    }
}
