﻿using Pilz.Plugins.Advanced.UI;
using Pilz.UI.Telerik;
using Pilz.UI.Telerik.Dialogs;
using System.Drawing;
using System.Windows.Forms;
using Telerik.WinControls;

namespace Pilz.Plugins.Advanced.UI.Telerik
{
    public abstract class PluginModule : PluginModule<PluginModuleUI>
    {
        /// <summary>
        /// Wrapper for the <see cref="PluginFeature.Icon"/> property to directly use it as <see cref="RadSvgImage"/>.
        /// </summary>
        public RadSvgImage? SvgImage
        {
            get => base.Icon as RadSvgImage;
            set => base.Icon = value;
        }

        protected PluginModule(string moduleType, string moduleIdentifier) : base(moduleType, moduleIdentifier)
        {

        }

        protected PluginModule(string moduleType, string moduleIdentifier, string moduleName) : base(moduleType, moduleIdentifier, moduleName)
        {
        }

        public override void ShowUI()
        {
            if (CreateNewUI() is PluginModuleUI ui)
            {
                ui.BackColor = Color.Transparent;
                DialogBase.Show(ui, Name!, SvgImage!.ToImage().ToIcon()!);
            }
        }
    }
}