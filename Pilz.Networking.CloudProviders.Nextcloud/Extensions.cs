﻿using Pilz.Networking.CloudProviders.Nextcloud.Ocs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Networking.CloudProviders.Nextcloud
{
    public static class Extensions
    {
        public static string ToBasicAuth(this OcsApiAuthCredentials? credentials)
        {
            if (credentials != null)
            {
                var creds = $"{credentials?.LoginName}:{credentials?.AppPassword}";
                var bytes = Encoding.UTF8.GetBytes(creds);
                return "Basic " + Convert.ToBase64String(bytes);
            }

            return string.Empty;
        }

        public static OcsApiAuthCredentials? ToOcsApiAuthCredentials(this NextcloudLogin? login)
        {
            if (!string.IsNullOrEmpty(login?.LoginName) && !string.IsNullOrEmpty(login?.AppPassword))
                return new OcsApiAuthCredentials(login.LoginName, login.AppPassword);
            return null;
        }

        public static OcsApiUrlPath FillParameters(this OcsApiUrlPath path, params object?[] @params)
        {
            return (OcsApiUrlPath)string.Format(path, @params);
        }

        public static DateTime UnixTimeMillisecondsToDateTime(this long? value)
        {
            return DateTimeOffset.FromUnixTimeMilliseconds(value ?? 0).DateTime;
        }

        public static long ToUnixTimeMilliseconds(this DateTime value)
        {
            return new DateTimeOffset(value).ToUnixTimeMilliseconds();
        }
    }
}
