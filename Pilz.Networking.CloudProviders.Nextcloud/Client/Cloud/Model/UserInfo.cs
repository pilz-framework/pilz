﻿using Pilz.Networking.CloudProviders.Nextcloud.Client.Cloud.Ocs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Networking.CloudProviders.Nextcloud.Client.Cloud.Model
{
    public class UserInfo
    {
        /// <summary>
        /// Defines if the user is enabled.
        /// </summary>
        public bool Enabled { get; init; }

        /// <summary>
        /// The location of the user's storage directory.
        /// </summary>
        public string? StorageLocation { get; init; }

        /// <summary>
        /// The uniquie user id that infos are for.
        /// </summary>
        public string? ID { get; init; }

        /// <summary>
        /// The last time when the user has logged in to its account.
        /// </summary>
        public DateTime LastLogin { get; init; }

        /// <summary>
        /// The backend of the user. Common values are "Database" or "LDAP".
        /// </summary>
        public string? Backend { get; init; }

        /// <summary>
        /// The Email address of the user.
        /// </summary>
        public string? Email { get; init; }

        /// <summary>
        /// The displayname of the user.
        /// </summary>
        public string? Displayname { get; init; }

        /// <summary>
        /// The displayname of the user.
        /// </summary>
        public string? Displayname2 { get; init; }

        /// <summary>
        /// The phone number of the user.
        /// </summary>
        public string? Phone { get; init; }

        /// <summary>
        /// The address of the user.
        /// </summary>
        public string? Address { get; init; }

        /// <summary>
        /// The Website of the user.
        /// </summary>
        public string? Website { get; init; }

        /// <summary>
        /// The twitter profile name of the user.
        /// </summary>
        public string? Twitter { get; init; }

        /// <summary>
        /// Defines the groups the user is member of.
        /// </summary>
        public string[] Groups { get; init; }

        /// <summary>
        /// The configured language of the user.
        /// </summary>
        public string? Language { get; init; }

        /// <summary>
        /// The configured location of the user.
        /// </summary>
        public string? Locale { get; init; }

        /// <summary>
        /// Quota informations for the user.
        /// </summary>
        public UserQuota Quota { get; } = new();

        /// <summary>
        /// Backend capabilities of the user.
        /// </summary>
        public UserBackendCapabilities BackendCapabilities { get; } = new();

        public UserInfo(OcsResponseDataUser responseData)
        {
            Enabled = Convert.ToBoolean(responseData.Enabled);
            StorageLocation = responseData.StorageLocation;
            ID = responseData.ID;
            LastLogin = responseData.LastLogin.UnixTimeMillisecondsToDateTime();
            Backend = responseData.Backend;
            Email = responseData.Email;
            Displayname = responseData.Displayname;
            Displayname2 = responseData.Displayname2;
            Phone = responseData.Phone;
            Address = responseData.Address;
            Website = responseData.Website;
            Twitter = responseData.Twitter;
            Groups = responseData.Groups ?? Array.Empty<string>();
            Language = responseData.Language;
            Locale = responseData.Locale;

            if (responseData.Quota != null)
            {
                Quota.Free = responseData.Quota.Free ?? 0;
                Quota.Used = responseData.Quota.Used ?? 0;
                Quota.Total = responseData.Quota.Total ?? 0;
                Quota.Relative = responseData.Quota.Relative ?? 0.0F;
                Quota.Quota = responseData.Quota.Quota ?? 0;
            }

            if (responseData.BackendCapabilities != null)
            {
                BackendCapabilities.SetDisplayName = Convert.ToBoolean(responseData.BackendCapabilities.SetDisplayName);
                BackendCapabilities.SetPassword = Convert.ToBoolean(responseData.BackendCapabilities.SetPassword);
            }
        }
    }
}
