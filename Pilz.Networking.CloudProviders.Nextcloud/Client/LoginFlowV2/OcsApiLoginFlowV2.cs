﻿using Pilz.Networking.CloudProviders.Nextcloud.Client.LoginFlowV2.Ocs;
using Pilz.Networking.CloudProviders.Nextcloud.Ocs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Networking.CloudProviders.Nextcloud.Client.LoginFlowV2
{
    public class OcsApiLoginFlowV2 : OcsApiBase
    {
        private const string OCS_LOGIN_INIT = "/index.php/login/v2";

        public OcsApiLoginFlowV2(OcsApi manager) : base(manager)
        {
        }

        public OcsResponseLoginFlowV2? Init(string url)
        {
            return Manager.MakeRequest<OcsResponseLoginFlowV2>(HttpMethod.Post, url + OCS_LOGIN_INIT);
        }

        public OcsResponseLoginFlowV2Credentials? Poll(OcsResponseLoginFlowV2.PollData poll)
        {
            ArgumentNullException.ThrowIfNull(poll?.Endpoint);
            ArgumentNullException.ThrowIfNull(poll?.Token);

            return Manager.MakeRequest<OcsResponseLoginFlowV2Credentials?>(HttpMethod.Post, poll.Endpoint,
                parameters: new Dictionary<string, string>
                {
                    { "token", poll.Token }
                });
        }
    }
}
