﻿using Pilz.Networking.CloudProviders.Nextcloud.Client.Apps.Tables.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Networking.CloudProviders.Nextcloud.Client.Apps.Tables
{
    public class TablesClient : ClientBase
    {
        public TablesClient(NextcloudClient client) : base(client)
        {
        }

        public RowsSimple? GetRowsSimple(long tableId)
        {
            return Client.Ocs.GetApi<OcsApiTables>().GetRowsSimple(tableId);
        }

        public Rows? GetRows(long tableId)
        {
            return Client.Ocs.GetApi<OcsApiTables>().GetRows(tableId);
        }

        public Rows? GetViewRows(long viewId)
        {
            return Client.Ocs.GetApi<OcsApiTables>().GetViewRows(viewId);
        }

        public Row? GetRow(long rowId)
        {
            return Client.Ocs.GetApi<OcsApiTables>().GetRow(rowId);
        }

        public Columns? GetColumns(long tableId)
        {
            return Client.Ocs.GetApi<OcsApiTables>().GetColumns(tableId);
        }

        public Columns? GetViewColumns(long viewId)
        {
            return Client.Ocs.GetApi<OcsApiTables>().GetViewColumns(viewId);
        }

        public Column? GetColumn(long columnId)
        {
            return Client.Ocs.GetApi<OcsApiTables>().GetColumn(columnId);
        }

        public bool DeleteRow(long rowId)
        {
            return DeleteRowAdv(rowId) is not null;
        }

        public Row? DeleteRowAdv(long rowId)
        {
            return Client.Ocs.GetApi<OcsApiTables>().DeleteRow(rowId);
        }

        public bool DeleteColumn(long columnId)
        {
            return DeleteColumnAdv(columnId) is not null;
        }

        public Column? DeleteColumnAdv(long columnId)
        {
            return Client.Ocs.GetApi<OcsApiTables>().DeleteColumn(columnId);
        }

        public Row? UpdateRow(long rowId, RowUpdate values)
        {
            return Client.Ocs.GetApi<OcsApiTables>().UpdateRow(rowId, values);
        }

        public Row? CreateRow(long tableId, RowUpdate values)
        {
            return Client.Ocs.GetApi<OcsApiTables>().CreateRow(tableId, values);
        }
    }
}
