﻿using Pilz.Networking.CloudProviders.Nextcloud.Client.Apps.FileRetention.Ocs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Networking.CloudProviders.Nextcloud.Client.Apps.FileRetention.Model
{
    public class RetentionRule : RetentionRuleInfo
    {
        /// <summary>
        /// The ID for the retention rule.
        /// </summary>
        public int ID { get; init; }

        /// <summary>
        /// Defines if a background job has been generated
        /// </summary>
        public bool HasJob { get; init; }

        public RetentionRule()
        {
        }

        public RetentionRule(OcsResponseDataEntryRetention data)
        {
            ID = data.ID ?? -1;
            TagID = data.TagID ?? -1;
            TimeUnit = (RetentionTimeUnit)(data.TimeUnit ?? 0);
            TimeAmount = data.TimeAmount ?? -1;
            TimeAfter = (RetentionTimeAfter)(data.TimeAfter ?? 0);
            HasJob = data.HasJob ?? false;
        }
    }
}
