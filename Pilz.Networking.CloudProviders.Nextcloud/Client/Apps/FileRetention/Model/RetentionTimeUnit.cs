﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Networking.CloudProviders.Nextcloud.Client.Apps.FileRetention.Model
{
    public enum RetentionTimeUnit
    {
        Day,
        Week,
        Month,
        Year
    }
}
