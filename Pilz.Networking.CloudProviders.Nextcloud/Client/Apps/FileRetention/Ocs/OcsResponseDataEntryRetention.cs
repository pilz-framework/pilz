﻿using Newtonsoft.Json;
using Pilz.Networking.CloudProviders.Nextcloud.Ocs.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Networking.CloudProviders.Nextcloud.Client.Apps.FileRetention.Ocs
{
    public class OcsResponseDataEntryRetention : OcsResponseDataEntry
    {
        [JsonProperty("id")]
        public int? ID { get; set; }

        [JsonProperty("tagid")]
        public int? TagID { get; set; }

        [JsonProperty("timeunit")]
        public int? TimeUnit { get; set; }

        [JsonProperty("timeamount")]
        public int? TimeAmount { get; set; }

        [JsonProperty("timeafter")]
        public int? TimeAfter { get; set; }

        [JsonProperty("hasJob")]
        public bool? HasJob { get; set; }
    }
}
