﻿using Newtonsoft.Json;
using Pilz.Networking.CloudProviders.Nextcloud.Ocs.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Networking.CloudProviders.Nextcloud.Client.Apps.FileRetention.Ocs
{
    public class OcsResponseRetention : OcsResponse<OcsResponseDataArray<OcsResponseDataEntryRetention>>
    {
    }
}
