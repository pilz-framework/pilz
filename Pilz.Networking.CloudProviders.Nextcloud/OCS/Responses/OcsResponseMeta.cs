﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Networking.CloudProviders.Nextcloud.Ocs.Responses
{
    public class OcsResponseMeta : IOcsResponseMeta
    {
        [JsonProperty("status")]
        public string? Status { get; set; }

        [JsonProperty("statuscode")]
        public int? StatusCode { get; set; }

        [JsonProperty("message")]
        public string? Message { get; set; }
    }
}
