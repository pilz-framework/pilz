﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.Window;

namespace Pilz.UI.Telerik.Dialogs
{
    partial class DialogBase
	{
		public delegate void DialogLoadingEventHandler(DialogLoadingEventArgs e);
		public delegate void DialogClosedEventHandler(DialogClosedEventArgs e);

		public static event DialogLoadingEventHandler? DialogLoading;
		public static event DialogClosedEventHandler? DialogClosed;

		public static T Show<T>(string title, Icon icon, object? tag = null) where T : FlyoutBase
        {
            return Show<T>(null, title, icon, tag);
        }

		public static T ShowDialog<T>(string title, Icon icon, object? tag = null) where T : FlyoutBase
		{
			return ShowDialog<T>(null, title, icon, tag);
        }

        public static T Show<T>(IWin32Window? parent, string title, Icon icon, object? tag = null) where T : FlyoutBase
        {
            return Show(CreatePanelInstance<T>(tag), parent, title, icon);
        }

        public static T ShowDialog<T>(IWin32Window? parent, string title, Icon icon, object? tag = null) where T : FlyoutBase
		{
            return ShowDialog(CreatePanelInstance<T>(tag), parent, title, icon);
        }

        public static T Show<T>(T dialogPanel, string title, Icon icon) where T : FlyoutBase
        {
            return Show(dialogPanel, null, title, icon);
        }

        public static T ShowDialog<T>(T dialogPanel, string title, Icon icon) where T : FlyoutBase
        {
            return ShowDialog(dialogPanel, null, title, icon);
        }

        public static T Show<T>(T dialogPanel, IWin32Window? parent, string title, Icon icon) where T : FlyoutBase
        {
            CreateForm(dialogPanel, parent, title, icon).Show();
            return dialogPanel;
        }

        public static T ShowDialog<T>(T dialogPanel, IWin32Window? parent, string title, Icon icon) where T : FlyoutBase
        {
            CreateForm(dialogPanel, parent, title, icon).ShowDialog();
            return dialogPanel;
        }

        private static T CreatePanelInstance<T>(object? tag) where T : FlyoutBase
        {
            T dialogPanel = Activator.CreateInstance<T>();
            dialogPanel.Tag = tag;
            return dialogPanel;
        }

        private static DialogBase CreateForm<T>(T dialogPanel, IWin32Window? parent, string title, Icon icon) where T : FlyoutBase
        {
            dialogPanel.Dock = DockStyle.Fill;

            var dialog = new DialogBase(dialogPanel)
            {
                Text = title,
                Icon = icon,
                StartPosition = parent == null ? FormStartPosition.CenterScreen : FormStartPosition.CenterParent,
                ClientSize = dialogPanel.Size
            };

            dialog.Controls.Add(dialogPanel);

            return dialog;
        }
	}
}
