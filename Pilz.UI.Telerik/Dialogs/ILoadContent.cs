﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.UI.Telerik.Dialogs
{
    public interface ILoadContent
    {
        void LoadContent();
    }
}
