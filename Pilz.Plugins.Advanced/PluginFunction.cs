﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Plugins.Advanced
{
    public abstract class PluginFunction : PluginFeature
    {
        protected PluginFunction(string functionType, string functionIdentifier) : base(functionType, functionIdentifier)
        {
        }

        protected PluginFunction(string functionType, string functionIdentifier, string? functionName) : base(functionType, functionIdentifier, functionName)
        {
        }

        public virtual object? Execute()
        {
            return Execute((PluginFunctionParameter?)null);
        }

        public virtual T? Execute<T>(params object?[]? @params)
        {
            return Execute<T>(new PluginFunctionSimpleParamter(@params));
        }

        public virtual object? Execute(params object?[]? @params)
        {
            return Execute(new PluginFunctionSimpleParamter(@params));
        }

        public virtual T? Execute<T>(PluginFunctionSimpleParamter? @params)
        {
            if (Execute(@params) is T result)
                return result;
            return default;
        }

        public virtual object? Execute(PluginFunctionParameter? @params)
        {
            return ExecuteFunction(@params);
        }

        protected abstract object? ExecuteFunction(PluginFunctionParameter? @params);
    }
}
