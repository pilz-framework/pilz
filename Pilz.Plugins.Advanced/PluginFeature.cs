﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Plugins.Advanced
{
    public abstract class PluginFeature
    {
        /// <summary>
        /// The type of the feature defines where the feature get integrated.
        /// </summary>
        public string Type { get; init; }
        /// <summary>
        /// The identifier of the feature should be uniquie for the current <see cref="Type"/>.
        /// It defines a feature within a type.
        /// </summary>
        public string Identifier { get; init; }
        /// <summary>
        /// The full identifier of the feature should be uniquie and is the combination of <see cref="Type"/> and <see cref="Identifier"/>.
        /// It defines a feature across all types.
        /// </summary>
        public string FullIdentifier => GetFullIdentifier(Type, Identifier);
        /// <summary>
        /// The (display) name of the feature.
        /// </summary>
        public virtual string? Name { get; init; }
        /// <summary>
        /// The symbol for the feature.
        /// </summary>
        public virtual object? Icon { get; set; }
        /// <summary>
        /// Sets the prioritization of the feature.
        /// This will be respected on abfragen features and on inserting as items using the extension methods"/>.
        /// Some applications might implement a way to regonize feature prioritization via its own way.
        /// </summary>
        public virtual FeaturePrioritization Prioritization { get; set; }
        /// <summary>
        /// Defines if the feature is enabled/visible.
        /// </summary>
        public virtual bool Enabled { get; set; } = true;

        protected PluginFeature(string featureType, string identifier)
        {
            Identifier = identifier;
            Type = featureType;
        }

        protected PluginFeature(string featureType, string featureIdentifier, string? featureName) : this(featureType, featureIdentifier)
        {
            Name = featureName;
        }

        public static string GetFullIdentifier(string featureType, string identifier)
        {
            return $"{featureType}:{identifier}";
        }
    }
}
