﻿using System.IO;
using Newtonsoft.Json;
using Pilz.GeneralEventArgs;

namespace Pilz.Configuration
{
    public class SimpleConfiguration
    {
        [JsonIgnore]
        public readonly ConfigurationManagerList Managers = new ConfigurationManagerList();

        public SimpleConfiguration()
        {
            Managers.GettingParentManager += Managers_GettingParentManager;
        }

        private void Managers_GettingParentManager(object sender, GetValueEventArgs<SimpleConfiguration> e)
        {
            if (ReferenceEquals(sender, Managers))
                e.Value = this;
        }

        /// <summary>
        /// Writes this instance to a string and return it.
        /// </summary>
        /// <returns>The content of the configuration instance as string.</returns>
        public string WriteToString()
        {
            return ConfigurationSerializer.WriteToString(this);
        }

        /// <summary>
        /// Write this instance to a given stream.
        /// </summary>
        /// <param name="stream">The stream where the content should be written to.</param>
        public void WriteToStream(Stream stream)
        {
            ConfigurationSerializer.WriteToStream(this, stream);
        }

        /// <summary>
        /// Writes this instance to the given filePath as text file.
        /// </summary>
        /// <param name="filePath">The file path where the content should be written to. The file will be created or overwritten.</param>
        public void WriteToFile(string filePath)
        {
            ConfigurationSerializer.WriteToFile(this, filePath);
        }

        /// <summary>
        /// Reads a configuratin from the given string and returns an instance of it.
        /// </summary>
        /// <typeparam name="T">The type of the configuration class to instance.</typeparam>
        /// <param name="content">The content of the configuration as string.</param>
        /// <returns></returns>
        public static T ReadFromString<T>(string content) where T : SimpleConfiguration
        {
            return ConfigurationSerializer.ReadFromString<T>(content);
        }

        /// <summary>
        /// Read a configuration from the given string and put them to this instance.
        /// </summary>
        /// <param name="content">The content of the configuration as string.</param>
        public void ReadFromString(string content)
        {
            ConfigurationSerializer.ReadFromString(this, content);
        }

        /// <summary>
        /// Reads a configuratin from the given string and returns an instance of it.
        /// </summary>
        /// <typeparam name="T">The type of the configuration class to instance.</typeparam>
        /// <param name="stream">The stream with the content of the configuration.</param>
        /// <returns></returns>
        public static T ReadFromStream<T>(Stream stream) where T : SimpleConfiguration
        {
            return ConfigurationSerializer.ReadFromStream<T>(stream);
        }

        /// <summary>
        /// Read a configuration from the given string and put them to this instance.
        /// </summary>
        /// <param name="stream">The stream with the content of the configuration.</param>
        public void ReadFromStream(Stream stream)
        {
            ConfigurationSerializer.ReadFromStream(this, stream);
        }

        /// <summary>
        /// Reads a configuratin from the given string and returns an instance of it.
        /// </summary>
        /// <typeparam name="T">The type of the configuration class to instance.</typeparam>
        /// <param name="filePath">The path to the file with the content of the configuration.</param>
        /// <returns></returns>
        public static T ReadFromFile<T>(string filePath) where T : SimpleConfiguration
        {
            return ConfigurationSerializer.ReadFromFile<T>(filePath);
        }

        /// <summary>
        /// Read a configuration from the given string and put them to this instance.
        /// </summary>
        /// <param name="filePath">The path to the file with the content of the configuration.</param>
        public void ReadFromFile(string filePath)
        {
            ConfigurationSerializer.ReadFromFile(this, filePath);
        }
    }
}