﻿Public Class PointD

    Public Property X As Double
    Public Property Y As Double

    Public Sub New()
        X = 0
        Y = 0
    End Sub

    Public Sub New(x As Double, y As Double)
        Me.X = x
        Me.Y = y
    End Sub

    Public Shared ReadOnly Property Empty As PointD
        Get
            Return New PointD
        End Get
    End Property

    Public Shared Operator =(val1 As PointD, val2 As PointD) As Boolean
        Return val1.X = val2.X AndAlso val1.Y = val2.Y
    End Operator

    Public Shared Operator <>(val1 As PointD, val2 As PointD) As Boolean
        Return val1.X <> val2.X OrElse val1.Y <> val2.Y
    End Operator


End Class
