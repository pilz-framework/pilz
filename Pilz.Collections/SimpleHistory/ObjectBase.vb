﻿Namespace SimpleHistory

    Public Class ObjectBase

        Public Shared Property DefaultPriorityValue As Integer = 1000

        Public Property UndoPriority As Integer = DefaultPriorityValue
        Public Property RedoPriority As Integer = DefaultPriorityValue

    End Class


End Namespace