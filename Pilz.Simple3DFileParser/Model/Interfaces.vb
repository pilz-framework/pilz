﻿Public Interface IToObject3D

    Function ToObject3D() As Object3D
    Function ToObject3DAsync() As Task(Of Object3D)

End Interface
