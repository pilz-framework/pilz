﻿Public Class Point
    Public Property Vertex As Vertex = Nothing
    Public Property UV As UV = Nothing
    Public Property VertexColor As VertexColor = Nothing
    Public Property Normal As Normal = Nothing
End Class
