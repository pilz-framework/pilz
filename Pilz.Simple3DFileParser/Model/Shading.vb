﻿Public Class Shading
    Public Property AmbientColor As Color = Color.FromArgb(&HFFFFFFFF)
    Public Property DiffuseColor As Color = Color.FromArgb(&HFF7F7F7F)
    Public Property DiffusePosition As Vertex = Nothing
End Class
