﻿Imports System.Drawing
Imports Pilz.Win32.Native

Namespace Mapped

    Public Class NativeFileInfo

        Public ReadOnly Property Icon As Icon
        Public ReadOnly Property SystemIconIndex As Integer
        Public ReadOnly Property Displayname As String
        Public ReadOnly Property Typename As String

        Public Sub New(info As SHFILEINFO)
            Icon = Icon.FromHandle(info.hIcon)
            SystemIconIndex = info.iIcon
            Displayname = info.szDisplayName
            Typename = info.szTypeName
        End Sub

    End Class

End Namespace
