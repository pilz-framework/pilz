﻿Imports Pilz.Win32.Native

Imports Keys = System.Windows.Forms.Keys

Namespace Mapped

    Public Class Keyboard

        Public Shared Function IsKeyDown(keyCode As Integer) As Boolean
            Return (User32.GetKeyState(keyCode) & &H8000) < 0
        End Function

        Public Shared Function IsKeyDown(keyCode As Keys) As Boolean
            Return IsKeyDown(CInt(keyCode))
        End Function

    End Class

End Namespace
