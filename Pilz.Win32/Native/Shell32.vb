﻿Imports System.Runtime.InteropServices

Namespace Native

    Public Class Shell32

        ''' <summary>
        '''     Creates an array of handles to large or small icons extracted from
        '''     the specified executable file, dynamic-link library (DLL), or icon
        '''     file. 
        ''' </summary>
        ''' <param name="lpszFile">
        '''     Name of an executable file, DLL, or icon file from which icons will
        '''     be extracted.
        ''' </param>
        ''' <param name="nIconIndex">
        '''     <para>
        '''         Specifies the zero-based index of the first icon to extract. For
        '''         example, if this value is zero, the function extracts the first
        '''         icon in the specified file.
        '''     </para>
        '''     <para>
        '''         If this value is �1 and <paramrefname="phiconLarge"/> and
        '''         <paramrefname="phiconSmall"/> are both NULL, the function returns
        '''         the total number of icons in the specified file. If the file is an
        '''         executable file or DLL, the return value is the number of
        '''         RT_GROUP_ICON resources. If the file is an .ico file, the return
        '''         value is 1. 
        '''     </para>
        '''     <para>
        '''         Windows 95/98/Me, Windows NT 4.0 and later: If this value is a 
        '''         negative number and either <paramrefname="phiconLarge"/> or 
        '''         <paramrefname="phiconSmall"/> is not NULL, the function begins by
        '''         extracting the icon whose resource identifier is equal to the
        '''         absolute value of <paramrefname="nIconIndex"/>. For example, use -3
        '''         to extract the icon whose resource identifier is 3. 
        '''     </para>
        ''' </param>
        ''' <param name="phIconLarge">
        '''     An array of icon handles that receives handles to the large icons
        '''     extracted from the file. If this parameter is NULL, no large icons
        '''     are extracted from the file.
        ''' </param>
        ''' <param name="phIconSmall">
        '''     An array of icon handles that receives handles to the small icons
        '''     extracted from the file. If this parameter is NULL, no small icons
        '''     are extracted from the file. 
        ''' </param>
        ''' <param name="nIcons">
        '''     Specifies the number of icons to extract from the file. 
        ''' </param>
        ''' <returns>
        '''     If the <paramrefname="nIconIndex"/> parameter is -1, the
        '''     <paramrefname="phIconLarge"/> parameter is NULL, and the
        '''     <paramrefname="phiconSmall"/> parameter is NULL, then the return
        '''     value is the number of icons contained in the specified file.
        '''     Otherwise, the return value is the number of icons successfully
        '''     extracted from the file. 
        ''' </returns>
        <DllImport("Shell32", CharSet:=CharSet.Auto)>
        Public Shared Function ExtractIconEx(<MarshalAs(UnmanagedType.LPTStr)> ByVal lpszFile As String, ByVal nIconIndex As Integer, ByVal phIconLarge As IntPtr(), ByVal phIconSmall As IntPtr(), ByVal nIcons As Integer) As Integer
        End Function

        Public Declare Function SHGetFileInfo Lib "shell32.dll" (ByVal pszPath As String, ByVal dwFileAttributes As UInteger, ByRef psfi As SHFILEINFO, ByVal cbSizeFileInfo As UInteger, ByVal uFlags As UInteger) As IntPtr

    End Class

End Namespace
