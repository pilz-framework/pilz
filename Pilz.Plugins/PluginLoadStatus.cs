﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Plugins
{
    public enum PluginLoadStatus
    {
        None,
        Success,
        AlreadyLoaded,
        ErrorAtLoading,
        FileNotFound,
        NoValidPlugin
    }
}
