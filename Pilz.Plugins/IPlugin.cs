﻿namespace Pilz.Plugins
{
    public interface IPlugin
    {
        public string Name { get; }
    }
}