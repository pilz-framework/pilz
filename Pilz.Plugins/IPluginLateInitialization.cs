﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Plugins
{
    public interface IPluginLateInitialization
    {
        abstract void LateInit();
    }
}
